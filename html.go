package cocoprint

import (
	"fmt"
	"html/template"
	"os"
	"time"

	"github.com/leekchan/accounting"
)

func formatAsRupiah(v float64) string {
	ac := accounting.Accounting{Symbol: "Rp ", Precision: 2, Thousand: ".", Decimal: ","}
	return fmt.Sprintf("%s", ac.FormatMoney(v))
}

func formatAsNumber(v float64) string {
	ac := accounting.Accounting{Symbol: "", Thousand: ",", Decimal: "."}
	return fmt.Sprintf("%s", ac.FormatMoney(v))
}

func formatAsDate(t time.Time) string {
	year, month, day := t.Date()
	return fmt.Sprintf("%d/%d/%d", day, month, year)
}

func formatAsFullDate(t time.Time) string {
	s := t.Format("01 January 2006")
	return fmt.Sprintf("%s", s)
}

func mul(param1 float64, param2 float64) float64 {
	return param1 * param2
}

func add(param1 int, param2 int) int {
	return param1 + param2
}

func CreateTemplateToHTML(pdf PDF) (PDF, error) {

	var (
		err error
	)

	if pdf.FileName != "" && pdf.HeaderName != "" {
		pdf, err = generateTemplateHeader(pdf)
		if err != nil {
			return pdf, err
		}
	}

	pdf, err = generateTemplateContent(pdf)
	if err != nil {
		return pdf, err
	}

	return pdf, nil
}

func generateTemplateHeader(pdf PDF) (PDF, error) {
	var (
		err  error
		file *os.File
	)

	var tmpl *template.Template
	tmpl = compileTemplateWithData(pdf.HeaderName, pdf.HeaderPath)

	templateDataPath := pdf.FilePath + pdf.FileName + pdf.HeaderName
	if file, err = os.Create(templateDataPath); err != nil {
		fmt.Println(err)
		return pdf, err
	}

	if err = tmpl.Execute(file, pdf.Data); err != nil {
		fmt.Println(err)
		return pdf, err
	}

	file.Close()

	pdf.HeaderPath = pdf.FilePath
	pdf.HeaderName = pdf.FileName + pdf.HeaderName

	return pdf, nil

}

func generateTemplateContent(pdf PDF) (PDF, error) {

	var (
		err  error
		file *os.File
	)

	templateForPdf := []Template{}
	for _, htmlTemplate := range pdf.Templates {

		var tmpl *template.Template

		tmpl = compileTemplateWithData(htmlTemplate.Name, htmlTemplate.Path)

		templateDataPath := pdf.FilePath + pdf.FileName + htmlTemplate.Name
		if file, err = os.Create(templateDataPath); err != nil {
			fmt.Println(err)
			return pdf, err
		}

		if err = tmpl.Execute(file, pdf.Data); err != nil {
			fmt.Println(err)
			return pdf, err
		}

		file.Close()

		templateForPdf = append(templateForPdf, Template{
			Header: htmlTemplate.Header,
			Path:   pdf.FilePath,
			Name:   pdf.FileName + htmlTemplate.Name,
		})
	}

	pdf.Templates = templateForPdf

	return pdf, nil
}

func compileTemplateWithData(templateName, templatePath string) *template.Template {

	fmap := template.FuncMap{
		"formatAsDate":     formatAsDate,
		"formatAsRupiah":   formatAsRupiah,
		"formatAsNumber":   formatAsNumber,
		"formatAsFullDate": formatAsFullDate,
		"add":              add,
		"mul":              mul,
	}

	tmpl := template.Must(template.New(templateName).Funcs(fmap).ParseFiles(templatePath + templateName))

	return tmpl
}
