# COCOPRINT

Golang library for wapper github.com/SebastiaanKlippert/go-wkhtmltopdf

Golang commandline wrapper for wkhtmltopdf

See http://wkhtmltopdf.org/index.html for wkhtmltopdf docs.



# Usage
See testfile ```cocoprint_test.go```

```go

import "bitbucket."

func Test_cocoprint_Generate(t *testing.T) {

	teardownTestCase := setupTestCase(t)
	defer teardownTestCase(t)

	pwd, _ := os.Getwd()

	t.Run("generate template to html", func(t *testing.T) {
		pdf := PDF{
			FileName: "lease2",
			FilePath: pwd + "/temp/",
			Margin: Margin{
				MarginLeft:   20,
				MarginTop:    30,
				MarginBottom: 25,
				MarginRight:  20,
			},
			HeaderName: "LA_header.html",
			HeaderPath: pwd + "/template/",
			Templates: []Template{
				Template{Path: pwd + "/template/", Name: "LA_cover.html"},
				Template{Header: true, Path: pwd + "/template/", Name: "LA_appendix.html"},
				Template{Header: true, Path: pwd + "/template/", Name: "LA.html"},
			},
			Data: nil, // data is interface
		}

		linkPath, err := Generate(pdf)

		if (err != nil) != false {
			t.Errorf("cocoprint_Generate() error = %v, wantErr %v", err, false)
			return
		}

		linkPathExpected := pwd + "/temp/" + pdf.FileName + ".pdf"
		assert.Equal(t, linkPath, linkPathExpected, "they should be equal")

	})
}

```