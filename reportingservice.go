package cocoprint

func Generate(pdf PDF) (string, error) {
	var (
		err error
	)

	html, err := CreateTemplateToHTML(pdf)
	pdfPath, err := CreateHTMLToPDF(html)

	return pdfPath, err
}
