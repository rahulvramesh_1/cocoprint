package cocoprint

import "time"

type LeaseAgreementReport struct {
	ID                   int64         `json:"id,omitempty"`
	BookingID            int64         `json:"booking_id,omitempty"`
	BookingFacilityID    int64         `json:"booking_facility_id,omitempty"`
	FacilityID           int64         `json:"facility_id,omitempty"`
	StartDate            *time.Time    `json:"start_date,omitempty"`
	EndDate              *time.Time    `json:"end_date,omitempty"`
	FinalExpDate         *time.Time    `json:"final_exp_date,omitempty"`
	TerminationDate      *time.Time    `json:"termination_date,omitempty"`
	FinalRent            float64       `json:"final_rent,omitempty"`
	BaseRent             float64       `json:"base_rent,omitempty"`
	LeaseAgreementNumber string        `json:"lease_agreement_number"`
	Info                 Info          `json:"info,omitempty"`
	Tenant               Tenant        `json:"tenant,omitempty"`
	User                 User          `json:"users,omitempty"`
	BookingItem          BookingItem   `json:"booking_item,omitempty"`
	EvHive               EvHive        `json:"evhive,omitempty"`
	PaymentDetail        PaymentDetail `json:"payment_detail,omitempty"`
}

type EvHive struct {
	Name      string `json:"name"`
	Address   string `json:"address"`
	Phone     string `json:"phone"`
	AddressHQ string `json:"address_hq"`
	Email     string `json:"email"`
	Website   string `json:"website"`
	PIC       PIC    `json:"pic"`
}

type PIC struct {
	FirstName string `json:"first_name,omitempty"`
	LastName  string `json:"last_name,omitempty"`
	Title     string `json:"title,omitempty"`
	Email     string `json:"email,omitempty"`
	Phone     string `json:"phone,omitempty"`
}

type User struct {
	FirstName    string `json:"first_name,omitempty"`
	LastName     string `json:"last_name,omitempty"`
	Email        string `json:"email,omitempty"`
	PhoneNumber  string `json:"phone,omitempty"`
	Address      string `json:"address,omitempty"`
	Title        string `json:"title,omitempty"`
	IDCardNumber string `json:"id_card_number,omitempty"`
}

type Tenant struct {
	Company   Company   `json:"company"`
	Recipient Recipient `json:"recipient"`
	PIC       PICTenant `json:"pic"`
}

type PICTenant struct {
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Title     string `json:"title"`
	Email     string `json:"email"`
	Phone     string `json:"phone_number"`
}

type Recipient struct {
	ID           int    `json:"id"`
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	Title        string `json:"title"`
	Email        string `json:"email"`
	Phone        string `json:"phone_number"`
	Address      string `json:"address"`
	IDCardNumber string `json:"id_card_number"`
}

type Company struct {
	ID                         int        `json:"id,omitempty"`
	Name                       string     `json:"name,omitempty"`
	Address                    string     `json:"address,omitempty"`
	IncorporationDate          *time.Time `json:"incorporation_date,omitempty"`
	BusinessRegistrationNumber string     `json:"business_registration_number,omitempty"`
	DirectorName               string     `json:"director_name,omitempty"`
	IDCardNumber               string     `json:"id_card_number,omitempty"`
	PhoneNumber                string     `json:"phone_number,omitempty"`
	TaxRegistrationNumber      string     `json:"tax_registration_number,omitempty"`
}

type Info struct {
	LANumber string    `json:"serial_number"`
	Date     time.Time `json:"date"`
}

type BookingItem struct {
	ID                     int        `json:"id,omitempty"`
	Location               Location   `json:"location,omitempty"`
	Floor                  Floor      `json:"floor,omitempty"`
	Facility               Facility   `json:"facility,omitempty"`
	BookingBaseRent        float64    `json:"booking_base_rent,omitempty"`
	BookingFinalRent       float64    `json:"booking_final_rent,omitempty"`
	FreeMeetingRoomCredits int        `json:"free_meeting_room_credit,omitempty"`
	FreeFlexiDeskCount     int        `json:"free_flexi_desk_count,omitempty"`
	OtherDetail            string     `json:"other_detail,omitempty"`
	BookingLeaseLength     int        `json:"booking_lease_length,omitempty"`
	BookingStartDate       *time.Time `json:"start_date,omitempty"`
	BookingExpDate         *time.Time `json:"end_date,omitempty"`
	BookingExpDateFinal    *time.Time `json:"final_exp_date,omitempty"`
	FacilityDiscount       float64    `json:"facility_discount,omitempty"`
}

type Location struct {
	Name       string `json:"name,omitempty"`
	UniqueCode string `json:"unique_code,omitempty"`
	Building   string `json:"building,omitempty"`
	Address    string `json:"address,omitempty"`
	City       string `json:"city,omitempty"`
	State      string `json:"state,omitempty"`
	Country    string `json:"country,omitempty"`
	Zip        string `json:"zip_code,omitempty"`
}

type Floor struct {
	Floor string `json:"floor,omitempty"`
}

type Facility struct {
	ID                   int64      `json:"id,omitempty"`
	Name                 string     `json:"name,omitempty"`
	Size                 string     `json:"size,omitempty"`
	LocationId           int64      `json:"location_id,omitempty"`
	FacilityDiscount     float64    `json:"contract_length_discount,omitempty"`
	Capacity             int        `json:"capacity,omitempty"`               // capacity
	FacilitiesTypeName   string     `json:"facilities_type_name,omitempty"`   // facilities_type_name
	RoomNumber           string     `json:"room_number,omitempty"`            // room_number
	OpenOnStatus         int        `json:"open_on_status,omitempty"`         // open_on_status
	AvailabilityStatusID int        `json:"availability_status_id,omitempty"` // availability_status_id
	StatusID             int        `json:"status_id,omitempty"`              // status_id
	BankAccountNumber    string     `json:"bank_account_number,omitempty"`    // bank_account_number
	ValidDate            *time.Time `json:"valid_date,omitempty"`             // valid_date
}

type PaymentDetail struct {
	Terms                string  `json:"terms,omitempty"`
	RentalPaid           string  `json:"rental_paid,omitempty"`
	MethodID             int     `json:"payment_method_id,omitempty"`
	VendorID             int     `json:"payment_vendor_id,omitempty"`
	VendorChildID        int     `json:"payment_vendor_child_id,omitempty"`
	PromoCode            string  `json:"promo_code,omitempty"`
	PromoDiscount        string  `json:"promo_discount,omitempty"`
	FinalPricePerMonth   float64 `json:"final_price_monthly,omitempty"`
	DepositPeriod        int     `json:"deposit_period,omitempty"`
	DepositAmount        float64 `json:"deposit_amount,omitempty"`
	AdvancePaymentPeriod int     `json:"advance_payment_period,omitempty"`
	AdvancePaymentAmount float64 `json:"advance_payment_amount,omitempty"`
}
